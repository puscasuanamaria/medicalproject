package repository;

import model.BaseEntity;
import org.xml.sax.SAXException;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class InMemoryRepository<ID, T extends BaseEntity<ID>> implements Repository<ID, T> {
    private Map<ID, T> entities;

    public InMemoryRepository() {
        entities = new HashMap<>();
    }

    /**
     * Find the Entity with the given {@code id}.
     *
     * @param id must be not null.
     * @return a {@code Entity} with the given id.
     * @throws IllegalArgumentException if the given id is null.
     */
    @Override
    public Optional<T> findOne(ID id) {
        if (id == null) {
            throw new IllegalArgumentException("id must not be null");
        }
        return Optional.ofNullable(entities.get(id));
    }

    /**
     * Returns all entities from repository.
     *
     * @return a set of {@code Entities}.
     */
    @Override
    public Iterable<T> findAll() {
        Set<T> allEntities = entities.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toSet());
        return allEntities;
    }

    /**
     * Saves the given Entity.
     *
     * @param entity must not be null.
     * @return a {@code Entity} - null if the entity was saved, otherwise (e.g. id already exists) returns the entity.
     */
    @Override
    public Optional save(T entity) throws ValidatorException, IOException, SAXException, ParserConfigurationException {
        if (entity == null) {
            throw new ValidatorException("id must not be null");
        }
        return Optional.ofNullable(entities.putIfAbsent(entity.getId(), entity));
    }

    /**
     * Deletes all entities.
     */
    @Override
    public void deleteAll() {
        entities.clear();
    }
}
