package repository;

import model.Doctor;
import org.xml.sax.SAXException;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class DoctorRepository extends InMemoryRepository<Integer, Doctor> {
    private String fileName;

    public DoctorRepository(String fileName) {
        this.fileName = fileName;

        // Call loadData
        loadData();
    }

    /**
     * Load Data function is used in order to read data from a specific file.
     * This file contains all the generated doctor information.
     */
    private void loadData() {
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                int id = Integer.parseInt(items.get(0));
                String firstName = items.get(1);
                String lastName = items.get((2));
                Integer age = Integer.parseInt(items.get((3)));
                Integer shiftMin = Integer.parseInt(items.get((4)));

                Doctor doctor = new Doctor(id, firstName, lastName, age, shiftMin);
                doctor.setId(id);

                try {
                    super.save(doctor);
                } catch (ValidatorException | IOException | SAXException | ParserConfigurationException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Optional<Doctor> save(Doctor entity) throws ValidatorException, ParserConfigurationException, SAXException, IOException {
        Optional<Doctor> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    /**
     * SaveToFile function is used in order to save data to a specific file.
     * This function will save doctor information into the file.
     *
     * @param entity
     * @throws IOException
     */
    private void saveToFile(Doctor entity) throws IOException {
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId() + "," + entity.getFirstName() + "," + entity.getLastName() + "," + entity.getAge() + "," + entity.getShiftMin());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
