package repository;

import model.ConsultationReason;
import model.Patient;
import model.Priority;
import org.xml.sax.SAXException;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class PatientRepository extends InMemoryRepository<Integer, Patient> {
    private String fileName;

    public PatientRepository(String fileName) {
        this.fileName = fileName;

        // Call loadData
        loadData();
    }

    /**
     * LoadData function is used in order to read data from a specific file.
     * This file contains all the generated patient information.
     */
    private void loadData() {
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                int id = Integer.parseInt(items.get(0));
                String firstName = items.get(1);
                String lastName = items.get((2));
                Integer age = Integer.parseInt(items.get((3)));
                String stringConsultationReason = items.get((4));
                ConsultationReason consultationReason = ConsultationReason.valueOf(stringConsultationReason);
                String stringPriority = items.get((5));
                Priority priority = Priority.valueOf(stringPriority);

                Patient patient = new Patient(id, firstName, lastName, age, consultationReason, priority);
                patient.setId(id);

                try {
                    super.save(patient);
                } catch (ValidatorException | IOException | SAXException | ParserConfigurationException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Optional<Patient> save(Patient entity) throws ValidatorException, ParserConfigurationException, SAXException, IOException {
        Optional<Patient> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    /**
     * SaveToFile function is used in order to save data to a specific file.
     * This function will save patient information into the file.
     *
     * @param entity
     * @throws IOException
     */
    private void saveToFile(Patient entity) throws IOException {
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId() + "," + entity.getFirstName() + "," + entity.getLastName() + "," + entity.getAge() + ","
                            + entity.getConsultationReason() + "," + entity.getPriority());
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
