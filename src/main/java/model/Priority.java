package model;

public enum Priority {
    CHILDREN(0, 1),
    PUPIL(1, 7),
    STUDENT(7, 18),
    ADULT(18, 100);

    private final int startAge;
    private final int endAge;

    private Priority(int startAge, int endAge) {
        this.startAge = startAge;
        this.endAge = endAge;
    }

    public int getStartAge() {
        return startAge;
    }

    public int getEndAge() {
        return endAge;
    }
}