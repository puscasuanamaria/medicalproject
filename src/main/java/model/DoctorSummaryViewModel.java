package model;

public class DoctorSummaryViewModel {
    private String doctorFirstName;
    private String doctorLastName;
    private int doctorId;
    private int totalPacientes;
    private int totalWorkedMin;
    private int totalMoneyCollected;

    public DoctorSummaryViewModel(String doctorFirstName, String doctorLastName, int doctorId, int totalPacientes, int totalWorkedMin, int totalMoneyCollected) {
        this.doctorFirstName = doctorFirstName;
        this.doctorLastName = doctorLastName;
        this.doctorId = doctorId;
        this.totalPacientes = totalPacientes;
        this.totalWorkedMin = totalWorkedMin;
        this.totalMoneyCollected = totalMoneyCollected;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public int getTotalPacientes() {
        return totalPacientes;
    }

    public void setTotalPacientes(int totalPacientes) {
        this.totalPacientes = totalPacientes;
    }

    public int getTotalWorkedMin() {
        return totalWorkedMin;
    }

    public void setTotalWorkedMin(int totalWorkedMin) {
        this.totalWorkedMin = totalWorkedMin;
    }

    public int getTotalMoneyCollected() {
        return totalMoneyCollected;
    }

    public void setTotalMoneyCollected(int totalMoneyCollected) {
        this.totalMoneyCollected = totalMoneyCollected;
    }

    @Override
    public String toString() {
        return "DoctorSummaryViewModel{" +
                "doctorFirstName='" + doctorFirstName + '\'' +
                ", doctorLastName='" + doctorLastName + '\'' +
                ", doctorId=" + doctorId +
                ", totalPacientes=" + totalPacientes +
                ", totalWorkedMin=" + totalWorkedMin +
                ", totalMoneyCollected=" + totalMoneyCollected +
                '}';
    }
}
