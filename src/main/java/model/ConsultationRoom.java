package model;

public class ConsultationRoom extends BaseEntity<Integer> {
    private String name;
    private int minutesAvailable;
    private int occupiedBy;

    public ConsultationRoom(int id, String name, int occupiedBy) {
        super(id);
        this.name = name;
        this.minutesAvailable = 720;
        this.occupiedBy = occupiedBy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinutesAvailable() {
        return minutesAvailable;
    }

    public void setMinutesAvailable(int minutesAvailable) {
        this.minutesAvailable = minutesAvailable;
    }

    public int getOccupiedBy() {
        return occupiedBy;
    }

    public void setOccupiedBy(int occupiedBy) {
        this.occupiedBy = occupiedBy;
    }

    @Override
    public String toString() {
        return "ConsultationRoom{" +
                "name='" + name + '\'' +
                ", minutesAvailable=" + minutesAvailable +
                '}';
    }
}
