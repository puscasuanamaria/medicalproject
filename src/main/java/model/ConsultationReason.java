package model;

public enum ConsultationReason {
    CONSULTATION(30, 50),
    PRESCRIPTION(20, 20),
    TREATMENT(40, 35);

    private final int consultationTime;
    private final int price;

    private ConsultationReason(int consultationTime, int price) {
        this.consultationTime = consultationTime;
        this.price = price;
    }

    public int getConsultationTime() {
        return consultationTime;
    }

    public int getPrice() {
        return price;
    }
}