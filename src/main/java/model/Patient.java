package model;

public class Patient extends Person {
    private ConsultationReason consultationReason;
    private Priority priority;

    public Patient(int id, String firstName, String lastName, int age, ConsultationReason consultationReason, Priority priority) {
        super(id, firstName, lastName, age);
        this.consultationReason = consultationReason;
        this.priority = priority;
    }

    public ConsultationReason getConsultationReason() {
        return consultationReason;
    }

    public void setConsultationReason(ConsultationReason consultationReason) {
        this.consultationReason = consultationReason;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "firstName=" + getFirstName() +
                ", lastName=" + getLastName() +
                ", age=" + getAge() +
                ", consultationReason=" + consultationReason +
                ", priority=" + priority +
                '}';
    }
}