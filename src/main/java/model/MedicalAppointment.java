package model;

public class MedicalAppointment extends BaseEntity<Integer> {
    private int consultationRoomID;
    private int doctorID;
    private int patientID;

    public MedicalAppointment(Integer integer, int consultationRoomID, int doctorID, int patientID) {
        super(integer);
        this.consultationRoomID = consultationRoomID;
        this.doctorID = doctorID;
        this.patientID = patientID;
    }

    public int getConsultationRoomID() {
        return consultationRoomID;
    }

    public void setConsultationRoomID(int consultationRoomID) {
        this.consultationRoomID = consultationRoomID;
    }

    public int getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(int doctorID) {
        this.doctorID = doctorID;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }
}
