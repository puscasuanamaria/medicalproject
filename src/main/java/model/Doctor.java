package model;

public class Doctor extends Person {
    private int shiftDuration;

    public Doctor(int id, String firstName, String lastNAme, int age) {
        super(id, firstName, lastNAme, age);
        this.shiftDuration = 420;
    }

    public Doctor(int id, String firstName, String lastNAme, int age, int shiftMin) {
        super(id, firstName, lastNAme, age);
        this.shiftDuration = 420;
    }

    public int getShiftMin() {
        return shiftDuration;
    }

    public void setShiftMin(int shiftMin) {
        this.shiftDuration = shiftMin;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + getId() +
                ", firstName=" + getFirstName() +
                ", lastName=" + getLastName() +
                ", age=" + getAge() +
                ", shiftDuration=" + shiftDuration +
                '}';
    }
}
