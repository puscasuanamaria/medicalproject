package model.comparator;

import model.Patient;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient> {

    /**
     * Overriding compare()method of Comparator for ascending order based on Start Age
     *
     * @param p1
     * @param p2
     * @return
     */
    public int compare(Patient p1, Patient p2) {

        if (p1.getPriority().getStartAge() > p2.getPriority().getStartAge())
            return 1;
        else if (p1.getPriority().getStartAge() < p2.getPriority().getStartAge())
            return -1;
        return 0;
    }
}

