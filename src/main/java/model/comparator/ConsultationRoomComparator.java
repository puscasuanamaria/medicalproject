package model.comparator;

import model.ConsultationRoom;

import java.util.Comparator;

public class ConsultationRoomComparator implements Comparator<ConsultationRoom> {

    /**
     * Overriding compare()method of Comparator for descending order based on Minutes Available
     *
     * @param c1
     * @param c2
     * @return
     */
    public int compare(ConsultationRoom c1, ConsultationRoom c2) {

        if (c1.getMinutesAvailable() < c2.getMinutesAvailable())
            return 1;
        else if (c1.getMinutesAvailable() > c2.getMinutesAvailable())
            return -1;
        return 0;
    }
}
