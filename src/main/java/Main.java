import model.ConsultationRoom;
import model.Doctor;
import model.MedicalAppointment;
import model.Patient;
import org.xml.sax.SAXException;
import repository.*;
import service.ConsultationRoomService;
import service.DoctorService;
import service.MedicalAppoitmentService;
import service.PatientService;
import sun.security.validator.ValidatorException;
import ui.Console;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws SAXException, ValidatorException, ParserConfigurationException, IOException {

        try {
            System.out.println(new File("./data/").getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Repository<Integer, Doctor> doctorRepository = new DoctorRepository("./data/doctors.txt");
        DoctorService doctorService = new DoctorService(doctorRepository, "./data/doctors.txt");

        Repository<Integer, Patient> patientRepository = new PatientRepository("./data/patients.txt");
        PatientService patientService = new PatientService(patientRepository, "./data/patients.txt");

        Repository<Integer, ConsultationRoom> consultationRoomRepository = new InMemoryRepository<>();
        ConsultationRoomService consultationRoomService = new ConsultationRoomService(consultationRoomRepository);

        Repository<Integer, MedicalAppointment> medicalAppointmentRepository = new InMemoryRepository<>();
        Repository<Integer, Patient> patientsNotConsultedRepository = new InMemoryRepository<>();
        MedicalAppoitmentService medicalAppoitmentService = new MedicalAppoitmentService(medicalAppointmentRepository,
                consultationRoomRepository, doctorRepository, patientRepository, patientsNotConsultedRepository);

        Console console = new Console(doctorService, patientService, consultationRoomService, medicalAppoitmentService);
        console.runConsole();
    }
}