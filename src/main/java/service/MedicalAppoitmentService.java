package service;

import model.*;
import model.comparator.ConsultationRoomComparator;
import model.comparator.PatientComparator;
import org.xml.sax.SAXException;
import repository.Repository;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MedicalAppoitmentService {
    private Repository<Integer, MedicalAppointment> medicalAppointmentRepository;
    private Repository<Integer, ConsultationRoom> consultationRoomRepository;
    private Repository<Integer, Doctor> doctorRepository;
    private Repository<Integer, Patient> patientRepository;
    private Repository<Integer, Patient> patientsNotConsultedRepository;

    public MedicalAppoitmentService(Repository<Integer, MedicalAppointment> medicalAppointmentRepository,
                                    Repository<Integer, ConsultationRoom> consultationRoomRepository,
                                    Repository<Integer, Doctor> doctorRepository,
                                    Repository<Integer, Patient> patientRepository,
                                    Repository<Integer, Patient> patientsNotConsultedRepository) {
        this.medicalAppointmentRepository = medicalAppointmentRepository;
        this.consultationRoomRepository = consultationRoomRepository;
        this.doctorRepository = doctorRepository;
        this.patientRepository = patientRepository;
        this.patientsNotConsultedRepository = patientsNotConsultedRepository;
    }

    /**
     * Creates a Queue with a Priority for Patients.
     * The priority is based on Patient with lower Age.
     *
     * @return
     */
    public PriorityQueue<Patient> patientsPriorityQueue() {
        Iterable<Patient> patients = patientRepository.findAll();

        PriorityQueue<Patient> priorityQueue = new
                PriorityQueue<>(10, new PatientComparator());

        for (Patient patient : patients) {
            priorityQueue.add(patient);
        }
        return priorityQueue;
    }

    /**
     * Creates a Queue with a Priority for ConsultationRooms.
     * The priority is based on ConsultationRoom with the highest MinutesAvailable.
     *
     * @return
     */
    public PriorityQueue<ConsultationRoom> roomPriorityQueue() {
        Iterable<ConsultationRoom> rooms = consultationRoomRepository.findAll();

        PriorityQueue<ConsultationRoom> priorityQueue = new
                PriorityQueue<>(10, new ConsultationRoomComparator());

        for (ConsultationRoom room : rooms) {
            priorityQueue.add(room);
        }
        return priorityQueue;
    }

    /**
     * Creates a Map with MedicalAppointments.
     * This map is created based on the existing ConsultationRooms. The ConsultationRoom is available for 12 hours.
     * Each ConsultationRoom will have a Doctor allocated. This Doctor will be allocated to the ConsultationRoom
     * until his shift(of 7 hours) is over, or the ConsultationRoom becomes unavailable.
     * Each Doctor will receive Patients from a list that has already been ordered based on Patient Priority.
     * Each Patient will be received in the ConsultationRoom that has the highest MinutesAvailable.
     *
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws ValidatorException
     */
    public void addMedicalAppoitment() throws IOException, ParserConfigurationException, SAXException, ValidatorException {
        Iterable<Doctor> doctors = doctorRepository.findAll();

        PriorityQueue<Patient> patientsPriorityQueue = patientsPriorityQueue();

        Queue<Doctor> doctorsQueue = new LinkedList<>();
        for (Doctor doctor : doctors) {
            doctorsQueue.add(doctor);
        }

        PriorityQueue<ConsultationRoom> roomQueue = roomPriorityQueue();

        // Id for MedicalAppointment
        int i = 0;

        // Repe
        while (roomQueue.size() > 0) {
            ConsultationRoom indexRoom = roomQueue.peek();

            // If the ConsultationRoom has not been allocated to a Doctor
            if (indexRoom.getOccupiedBy() == 0) {
                Doctor indexDoctor = doctorsQueue.peek();
                indexRoom.setOccupiedBy(indexDoctor.getId());

                Patient indexPatient = patientsPriorityQueue.peek();

                // If the ConsultationRoom is available
                if (indexRoom.getMinutesAvailable() >= indexPatient.getConsultationReason().getConsultationTime()) {

                    // If the Doctor shift has not ended
                    if (indexDoctor.getShiftMin() >= indexPatient.getConsultationReason().getConsultationTime()) {

                        MedicalAppointment medicalAppointment = new MedicalAppointment(i, indexRoom.getId(), indexDoctor.getId(), indexPatient.getId());
                        medicalAppointmentRepository.save(medicalAppointment);
                        i++;

                        indexRoom.setMinutesAvailable(indexRoom.getMinutesAvailable() - indexPatient.getConsultationReason().getConsultationTime());
                        Optional<ConsultationRoom> room = consultationRoomRepository.findOne(indexRoom.getId());
                        room.ifPresent(e -> e.setMinutesAvailable(indexRoom.getMinutesAvailable()));
                        roomQueue.remove(indexRoom);
                        roomQueue.add(indexRoom);

                        indexDoctor.setShiftMin(indexDoctor.getShiftMin() - indexPatient.getConsultationReason().getConsultationTime());
                        Optional<Doctor> doctor = doctorRepository.findOne(indexDoctor.getId());
                        doctor.ifPresent(e -> e.setShiftMin(indexDoctor.getShiftMin()));
                        doctorsQueue.remove(indexDoctor);
                        doctorsQueue.add(indexDoctor);

                        // Remove Patient if the MedicalAppointment has been saved
                        patientsPriorityQueue.remove(indexPatient);
                    } else {
                        // Remove Doctor if shift is over
                        doctorsQueue.remove(indexDoctor);
                        indexRoom.setOccupiedBy(0);
                    }
                } else {
                    // Remove ConsultationRoom if is not available
                    roomQueue.remove(indexRoom);
                }
            } else {
                Patient indexPatient = patientsPriorityQueue.peek();

                Optional<Doctor> indexDoctor = doctorRepository.findOne(indexRoom.getOccupiedBy());

                if (indexRoom.getMinutesAvailable() >= indexPatient.getConsultationReason().getConsultationTime()) {
                    if (indexDoctor.get().getShiftMin() >= indexPatient.getConsultationReason().getConsultationTime()) {
                        MedicalAppointment medicalAppointment = new MedicalAppointment(i, indexRoom.getId(), indexDoctor.get().getId(), indexPatient.getId());
                        medicalAppointmentRepository.save(medicalAppointment);
                        i++;

                        Optional<ConsultationRoom> room = consultationRoomRepository.findOne(indexRoom.getId());
                        room.ifPresent(e -> e.setMinutesAvailable(indexRoom.getMinutesAvailable() - indexPatient.getConsultationReason().getConsultationTime()));
                        roomQueue.remove(indexRoom);
                        roomQueue.add(indexRoom);

                        indexDoctor.ifPresent(e -> e.setShiftMin(indexDoctor.get().getShiftMin() - indexPatient.getConsultationReason().getConsultationTime()));

                        // Remove Patient if the MedicalAppointment has been saved
                        patientsPriorityQueue.remove(indexPatient);
                    } else {
                        // Remove Doctor if shift is over
                        doctorsQueue.remove(indexDoctor);
                        indexRoom.setOccupiedBy(0);
                    }
                } else {
                    // Remove ConsultationRoom if is not available
                    roomQueue.remove(indexRoom);
                }
            }
        }

        // Create the Map with the Patients that were not consulted
        for (Patient element : patientsPriorityQueue) {
            patientsNotConsultedRepository.save(element);
        }
    }

    //not used
    public void groupDoctorsByMedicalAppoitment() {
        Iterable<MedicalAppointment> medicalAppointments = medicalAppointmentRepository.findAll();

        List<MedicalAppointment> medicalAppointmentsList =
                StreamSupport.stream(medicalAppointments.spliterator(), false)
                        .collect(Collectors.toList());

        Map<Object, Long> byby = medicalAppointmentsList.stream()
                .collect(Collectors.groupingBy(p -> p.getDoctorID(), Collectors.counting()));

        byby.forEach((ef, count) -> {
            System.out.println(ef + ":" + count + " patients");
        });
    }

    /**
     * Create a Summary Report with all Doctors, all Patients consulted, Minutes spent and Money collected.
     *
     * @return
     */
    public Map<Integer, DoctorSummaryViewModel> getDoctorSummaryReport() {
        Map<Integer, DoctorSummaryViewModel> docVM = new HashMap<>();

        // Get all Doctors and all MedicalAppointments
        Iterable<Doctor> doctors = doctorRepository.findAll();
        Iterable<MedicalAppointment> medicalAppointments = medicalAppointmentRepository.findAll();

        // Iterate Doctors repository and MedicalAppointments repository
        for (Doctor doctor : doctors) {
            for (MedicalAppointment medicalAppointment : medicalAppointments) {

                if (doctor.getId() == medicalAppointment.getDoctorID()) {
                    int doctorId = medicalAppointment.getDoctorID();
                    String doctorFirstName = doctorRepository.findOne(doctorId).get().getFirstName();
                    String doctorLastName = doctorRepository.findOne(doctorId).get().getLastName();

                    int patientId = medicalAppointment.getPatientID();
                    int workedMin = patientRepository.findOne(patientId).get().getConsultationReason().getConsultationTime();
                    int moneyCollected = patientRepository.findOne(patientId).get().getConsultationReason().getPrice();

                    if (!docVM.containsKey(doctorId)) {
                        DoctorSummaryViewModel doctorSummary = new DoctorSummaryViewModel(
                                doctorFirstName, doctorLastName, doctorId, 1, workedMin, moneyCollected);
                        docVM.put(doctorId, doctorSummary);
                    } else {
                        DoctorSummaryViewModel doctorSummary = docVM.get(doctorId);
                        doctorSummary.setTotalPacientes(doctorSummary.getTotalPacientes() + 1);
                        doctorSummary.setTotalWorkedMin(doctorSummary.getTotalWorkedMin() + workedMin);
                        doctorSummary.setTotalMoneyCollected(doctorSummary.getTotalMoneyCollected() + moneyCollected);

                        docVM.replace(doctorId, doctorSummary);
                    }
                }
            }

            if (!docVM.containsKey(doctor.getId())) {
                DoctorSummaryViewModel doctorSummary = new DoctorSummaryViewModel(
                        doctor.getFirstName(), doctor.getLastName(), doctor.getId(), 0, 0, 0);
                docVM.put(doctor.getId(), doctorSummary);
            }
        }

        return docVM;
    }

    public Set<Patient> getAllNotConsultedPatients() {
        Iterable<Patient> patients = patientsNotConsultedRepository.findAll();
        return StreamSupport.stream(patients.spliterator(), false).collect(Collectors.toSet());
    }
}
