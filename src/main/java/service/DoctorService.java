package service;

import model.Doctor;
import org.xml.sax.SAXException;
import repository.Repository;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class DoctorService {
    private Repository<Integer, Doctor> repository;
    private String fileName;

    public DoctorService(Repository<Integer, Doctor> repository, String fileName) {
        this.repository = repository;
        this.fileName = fileName;
    }

    /**
     * GenerateDoctors function will generate a random list of Doctors.
     *
     * @throws ParserConfigurationException
     * @throws ValidatorException
     * @throws SAXException
     * @throws IOException
     */
    public void generateDoctors() throws ParserConfigurationException, ValidatorException, SAXException, IOException {
        //clear memory
        repository.deleteAll();

        //clear list of doctors
        BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName));
        writer.write("");
        writer.flush();

        int nrOfAddedDoctors = 0;
        int nrOfDoctors = HelperService.getRandomNumberInRange(8, 12);

        while (nrOfAddedDoctors <= nrOfDoctors) {
            int id = HelperService.getRandomNumberInRange(1000, 9999);

            Iterable<Doctor> doctors = repository.findAll();

            boolean idExists = StreamSupport.stream(doctors.spliterator(), false)
                    .anyMatch(num -> num.getId().equals(id));

            if (idExists == false) {
                String firstName = HelperService.getRandomString(3);
                String lastName = HelperService.getRandomString(2);
                int age = HelperService.getRandomNumberInRange(30, 65);

                Doctor doctor = new Doctor(id, firstName, lastName, age);
                addDoctor(doctor);
                nrOfAddedDoctors++;
            }
        }
    }

    public void addDoctor(Doctor doctor) throws IOException, ParserConfigurationException, SAXException, ValidatorException {
        repository.save(doctor);
    }

    public Set<Doctor> getAllDoctors() {
        Iterable<Doctor> doctors = repository.findAll();
        return StreamSupport.stream(doctors.spliterator(), false).collect(Collectors.toSet());
    }
}
