package service;

import model.ConsultationReason;
import model.Priority;

import java.util.Random;
import java.util.Spliterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class HelperService {

    /**
     * Generate a random integer between two values.
     *
     * @param min
     * @param max
     * @return
     */
    public static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    /**
     * Generate a random string with a specified length.
     *
     * @param lenght
     * @return
     */
    public static String getRandomString(int lenght) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'

        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(lenght)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return generatedString;
    }

    /**
     * Generate a random ConsultationReason.
     *
     * @return
     */
    public static ConsultationReason generateRandomConsultationReason() {
        ConsultationReason[] values = ConsultationReason.values();
        int length = values.length;
        int randIndex = new Random().nextInt(length);
        return values[randIndex];
    }

    /**
     * Generate a random Priority.
     *
     * @return
     */
    public static Priority generateRandomPriority() {
        Priority[] values = Priority.values();
        int length = values.length;
        int randIndex = new Random().nextInt(length);
        return values[randIndex];
    }

    /**
     * Transforms an Iterable object into a Stream object.
     *
     * @param iterable
     * @param <T>
     * @return
     */
    public static <T> Stream<T> getStreamFromIterable(Iterable<T> iterable) {
        Spliterator<T>
                spliterator = iterable.spliterator();
        return StreamSupport.stream(spliterator, false);
    }
}