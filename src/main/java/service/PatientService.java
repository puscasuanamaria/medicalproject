package service;

import model.ConsultationReason;
import model.Patient;
import model.Priority;
import org.xml.sax.SAXException;
import repository.Repository;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.EnumMap;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class PatientService {
    private Repository<Integer, Patient> repository;
    private String fileName;

    public PatientService(Repository<Integer, Patient> repository, String fileName) {
        this.repository = repository;
        this.fileName = fileName;
    }

    /**
     * GeneratePatients function will generate a random list of Patients.
     *
     * @throws ParserConfigurationException
     * @throws ValidatorException
     * @throws SAXException
     * @throws IOException
     */
    public void generatePatients() throws ParserConfigurationException, ValidatorException, SAXException, IOException {

        // Clear memory
        repository.deleteAll();

        // Clear list of Patients
        BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName));
        writer.write("");
        writer.flush();

        int nrOfAddedPatients = 0;
        int nrOfPatients = 100; // Predefined list of 100 Patients

        while (nrOfAddedPatients < nrOfPatients) {
            int id = HelperService.getRandomNumberInRange(1000, 9999);

            Iterable<Patient> patients = repository.findAll();

            boolean idExists = StreamSupport.stream(patients.spliterator(), false)
                    .anyMatch(num -> num.getId().equals(id));

            if (!idExists) {
                String firstName = HelperService.getRandomString(5);
                String lastName = HelperService.getRandomString(4);
                ConsultationReason consultationReason = HelperService.generateRandomConsultationReason();
                Priority priority = HelperService.generateRandomPriority();
                int age = HelperService.getRandomNumberInRange(priority.getStartAge(), priority.getEndAge());

                Patient patient = new Patient(id, firstName, lastName, age, consultationReason, priority);
                addPatient(patient);
                nrOfAddedPatients++;
            }
        }
    }

    public void addPatient(Patient patient) throws IOException, ParserConfigurationException, SAXException, ValidatorException {
        repository.save(patient);
    }

    public Set<Patient> getAllPatients() {
        Iterable<Patient> patients = repository.findAll();
        return StreamSupport.stream(patients.spliterator(), false).collect(Collectors.toSet());
    }

    /**
     * Create a Report based on Group Age of the Patients.
     *
     * @return
     */
    public EnumMap<Priority, Long> groupPatientsByPriority() {
        Iterable<Patient> initialPatientsList = repository.findAll();

        Stream<Patient> list = HelperService.getStreamFromIterable(initialPatientsList);

        EnumMap<Priority, Long> map = list.collect(Collectors.groupingBy(Patient::getPriority, () -> new EnumMap<>(Priority.class),
                Collectors.counting()));
        return map;
    }
}
