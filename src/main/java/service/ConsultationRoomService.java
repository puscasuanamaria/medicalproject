package service;

import model.ConsultationRoom;
import org.xml.sax.SAXException;
import repository.Repository;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ConsultationRoomService {
    private Repository<Integer, ConsultationRoom> consultationRoomRepository;

    public ConsultationRoomService(Repository<Integer, ConsultationRoom> consultationRoomRepository) {
        this.consultationRoomRepository = consultationRoomRepository;
    }

    /**
     * CreateConsultationRoom function will generate 4 predefined ConsultationRooms.
     *
     * @throws ParserConfigurationException
     * @throws ValidatorException
     * @throws SAXException
     * @throws IOException
     */
    public void createConsultationRoom() throws ParserConfigurationException, ValidatorException, SAXException, IOException {
        ConsultationRoom consultationRoom1 = new ConsultationRoom(1, "room1", 0);
        addConsultationRoom(consultationRoom1);

        ConsultationRoom consultationRoom2 = new ConsultationRoom(2, "room2", 0);
        addConsultationRoom(consultationRoom2);

        ConsultationRoom consultationRoom3 = new ConsultationRoom(3, "room3", 0);
        addConsultationRoom(consultationRoom3);

        ConsultationRoom consultationRoom4 = new ConsultationRoom(4, "room4", 0);
        addConsultationRoom(consultationRoom4);
    }

    public void addConsultationRoom(ConsultationRoom consultationRoom) throws IOException, ParserConfigurationException, SAXException, ValidatorException {
        consultationRoomRepository.save(consultationRoom);
    }

    /**
     * Get all generated ConsultationRooms.
     *
     * @return
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     * @throws ValidatorException
     */
    public Set<ConsultationRoom> getAllConsultationRooms() throws ParserConfigurationException, IOException, SAXException, ValidatorException {
        createConsultationRoom();

        Iterable<ConsultationRoom> consultationRooms = consultationRoomRepository.findAll();
        return StreamSupport.stream(consultationRooms.spliterator(), false).collect(Collectors.toSet());
    }
}
