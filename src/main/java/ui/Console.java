package ui;

import model.*;
import org.xml.sax.SAXException;
import service.ConsultationRoomService;
import service.DoctorService;
import service.MedicalAppoitmentService;
import service.PatientService;
import sun.security.validator.ValidatorException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.EnumMap;
import java.util.Map;
import java.util.Set;

public class Console {
    private DoctorService doctorService;
    private PatientService patientService;
    private ConsultationRoomService consultationRoomService;
    private MedicalAppoitmentService medicalAppoitmentService;

    public Console(DoctorService doctorService,
                   PatientService patientService,
                   ConsultationRoomService consultationRoomService,
                   MedicalAppoitmentService medicalAppoitmentService) {
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.consultationRoomService = consultationRoomService;
        this.medicalAppoitmentService = medicalAppoitmentService;
    }

    private void showMenu() {
        System.out.println("1. Generate all input data - doctors, patients, consultation rooms and medical appointments");
        System.out.println("2. Doctors");
        System.out.println("3. Patients");

        System.out.println("x. Exit");
    }

    public void runConsole() throws IOException, ValidatorException, ParserConfigurationException, SAXException {
        while (true) {
            showMenu();

            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String option = bufferRead.readLine();
            switch (option) {
                case "1":
                    runGenerateInputData();
                    break;
                case "2":
                    runDoctors();
                    break;
                case "3":
                    runPatients();
                    break;
                case "x":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }

    private void runGenerateInputData() throws ParserConfigurationException, ValidatorException, SAXException, IOException {
        // Generate consulting rooms
        Set<ConsultationRoom> cons = consultationRoomService.getAllConsultationRooms();

        // Generate doctors
        doctorService.generateDoctors();

        // Generate patients
        patientService.generatePatients();

        // Generate medical appointments
        medicalAppoitmentService.addMedicalAppoitment();

        System.out.println("All input data was successfully generated!");
    }


    public void runDoctors() throws ParserConfigurationException, ValidatorException, SAXException, IOException {
        while (true) {
            System.out.println("1. Generate doctors - list");
            System.out.println("2. Show doctors - list");
            System.out.println("3. Show summary of all doctors");

            System.out.println("x. Exit");

            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String option = bufferRead.readLine();
            switch (option) {
                case "1":
                    runGenerateDoctorsList();
                    break;
                case "2":
                    runShowDoctorsList();
                    break;
                case "3":
                    runShowSummaryOfAllDoctors();
                    break;
                case "x":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }

    private void runGenerateDoctorsList() throws ParserConfigurationException, IOException, SAXException, ValidatorException {
        doctorService.generateDoctors();
    }

    private void runShowDoctorsList() {
        Set<Doctor> doctors = doctorService.getAllDoctors();
        doctors.stream().forEach(System.out::println);
    }

    private void runShowSummaryOfAllDoctors() throws ParserConfigurationException, ValidatorException, SAXException, IOException {
        Map<Integer, DoctorSummaryViewModel> doctorsSummary = medicalAppoitmentService.getDoctorSummaryReport();
        doctorsSummary.entrySet().stream().forEach(e -> System.out.println(e.getValue()));
    }

    public void runPatients() throws ParserConfigurationException, ValidatorException, SAXException, IOException {
        while (true) {
            System.out.println("1. Generate patients - list");
            System.out.println("2. Show patients - list");
            System.out.println("3. Show summary of all patients");
            System.out.println("4. Show not consulted patients");

            System.out.println("x. Exit");

            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String option = bufferRead.readLine();
            switch (option) {
                case "1":
                    runGeneratePatientsList();
                    break;
                case "2":
                    runShowPatientsList();
                    break;
                case "3":
                    runShowsummaryOfAllPatients();
                    break;
                case "4":
                    runNotConsultedPatients();
                    break;
                case "x":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }

    private void runGeneratePatientsList() throws ParserConfigurationException, IOException, SAXException, ValidatorException {
        patientService.generatePatients();
    }

    private void runShowPatientsList() {
        Set<Patient> patients = patientService.getAllPatients();
        patients.stream().forEach(System.out::println);
    }

    private void runShowsummaryOfAllPatients() {
        EnumMap<Priority, Long> patientsGroupByPriority = patientService.groupPatientsByPriority();

        System.out.println("Summary of all patients: " + patientsGroupByPriority);
    }

    private void runNotConsultedPatients() {
        Set<Patient> patients = medicalAppoitmentService.getAllNotConsultedPatients();
        patients.stream().forEach(System.out::println);
    }
}
